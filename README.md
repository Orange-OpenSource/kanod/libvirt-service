libvirt-broker is an OS image that can be used to provide libvirt guest machines on demand using the baremetalpool protocol.

The OS image is built with kanod-image-builder and initial configuration is done through kanod-configure configuration files.
