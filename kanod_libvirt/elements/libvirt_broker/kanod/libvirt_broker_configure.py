#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import typing
import bcrypt
import os
from pathlib import Path
import sys
import subprocess
import time

from kanod_configure import common

BROKER_ROOT='/var/lib/libvirt-broker'
BROKER_DEFAULTS='/etc/default/libvirt-broker'
NGINX_CONF='/etc/nginx'
QEMU_URI='qemu:///session'


def set_password(passwd: str):
    salt = bcrypt.gensalt(rounds=5)
    hash = bcrypt.hashpw(passwd.encode('utf-8'), salt).decode('utf-8')
    with open(f'{BROKER_ROOT}/auth', 'w', encoding='utf-8') as fd:
        fd.write(f'admin:{hash}\n')


def set_defaults(vars: typing.List[typing.Tuple[str, str]]):
    with open(BROKER_DEFAULTS, 'w', encoding='utf-8') as fd:
        for (var, value)  in vars:
            fd.write(f"{var}='{value}'\n")


def setup_certificates(key, cert):
    if key is not None and cert is not None:
        folder = Path(f'{BROKER_ROOT}')
        with open(folder.joinpath('key.pem'), 'w') as fd:
            fd.write(key)
            fd.write('\n')
        with open(folder.joinpath("cert.pem"), 'w') as fd:
            fd.write(cert)
            fd.write('\n')
    else:
        print('* No configuration for proxy')


def allow_bridges(bridges: typing.List[str]):
    with open('/etc/qemu/bridge.conf','w', encoding='utf-8') as fd:
        for bridge in bridges:
            fd.write(f'allow {bridge}\n')
        fd.write('\n')


def launch_proxy(ip_address):
    common.render_template(
        'libvirt_broker_proxy.tmpl',
        f'{NGINX_CONF}/sites-available/proxy',
        {'ip_address': ip_address})
    os.symlink(
        f'{NGINX_CONF}/sites-available/proxy',
        f'{NGINX_CONF}/sites-enabled/proxy')
    proc = subprocess.run(
        ['systemctl', 'enable', 'nginx'],
        stdout=sys.stdout, stderr=subprocess.STDOUT)
    proc = subprocess.run(
        ['systemctl', 'start', 'nginx'],
        stdout=sys.stdout, stderr=subprocess.STDOUT)
    if proc.returncode != 0:
        print('Configuring proxy failed.')
    time.sleep(20)
    # TODO(cregut) Restarting does not make sense as everything is
    # sequential but it makes it work...
    proc = subprocess.run(
        ['systemctl', 'restart', 'nginx'],
        stdout=sys.stdout, stderr=subprocess.STDOUT)


def configure_libvirt_service(arg: common.RunnableParams):
    libvirt_broker = arg.conf.get('libvirt_broker', None)
    if libvirt_broker is None:
        print('* No configuration found')
        return
    admin_password = libvirt_broker.get('admin', 'secret')
    service_ip = libvirt_broker.get('ip', None)
    service_key = libvirt_broker.get('key', None)
    service_cert = libvirt_broker.get('certificate', None)
    max_vm = libvirt_broker.get('max_vm', 16)
    mac_seed = libvirt_broker.get('mac_seed', 0)
    bridges = libvirt_broker.get('bridges', [])

    vars = [
        ('BROKER_BMC_ADDRESS', service_ip),
        ('POOL_ROOT', BROKER_ROOT),
        ('MAC_PREFIX', str(mac_seed)),
        ('QEMU_URI', QEMU_URI),
        ('MAX_VM', str(max_vm))]
    set_defaults(vars)
    set_password(admin_password)
    allow_bridges(bridges)
    setup_certificates(service_key, service_cert)
    launch_proxy(service_ip)
    subprocess.run(['systemctl', 'enable', '--now', 'libvirt-broker'])



common.register('Libvirt broker', 200, configure_libvirt_service)
